/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 2/12/12
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Prints to console.
 *
 * @param word {String} what to print to console.
 */
function say(word) {
	console.log(word);
}

/**
 * Execute function and pass in the value.
 *
 * @param someFunction {Function} Function to execute.
 * @param value {Object} Parameter to pass in.
 */
function execute(someFunction, value) {
	someFunction(value);
}

execute(say, "Hello world!");
execute(function(word) {console.log(word);}, "Hi there!");