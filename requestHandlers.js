/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 2/12/12
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */


function start() {
	console.log("Request handle 'start' was called.");

	function sleep(ms) {
		var startTime = new Date().getTime();
		while(new Date().getTime() < startTime + ms);
	}

	sleep(1000);
	return "Hello Start.";
}

function upload() {
	console.log("Request handler 'upload' was called.");
	return "Hello Upload.";
}

exports.start = start;
exports.upload = upload;

