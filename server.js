/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 2/12/12
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */


var http = require("http");
var url = require("url");

// Receive requestListener parameter,
// which automatically added to the
// `request` event.
//
// Returns server object which has methos `listen`.
//
//http.createServer(function(request, response) {
//	response.writeHead(200, {"Content-Type": "text/plain"});
//	response.write("Hello World");
//	response.end();
//}).listen(8888);

function start(route, handle) {
	function onRequest(request, response) {

		var pathname = url.parse(request.url).pathname;
		console.log("Request for " + pathname + " received.");

		var content = route(handle, pathname);

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write(content);
		response.end();
	}

	var server = http.createServer(onRequest);
	server.listen(8888);

	console.log("Server has started.");
}

exports.start = start;




