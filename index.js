/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 2/12/12
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */

var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {};

handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;
handle["/upload"] = requestHandlers.upload;

server.start(router.route, handle);

