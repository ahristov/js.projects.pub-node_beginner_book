/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 2/12/12
 * Time: 1:13 PM
 * To change this template use File | Settings | File Templates.
 */

function route(handle, pathname) {
	console.log("About to route a request for " + pathname);

	if (typeof handle[pathname] === "function") {
		return handle[pathname]();
	} else {
		console.log("No request handler found for " + pathname);
		return "404 Not found";
	}
}

exports.route = route;
