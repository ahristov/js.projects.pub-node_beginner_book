# Node beginner's book

Following the code from the Node beginner's book...

> http://www.nodebeginner.org/#hello-world


## Git setup

From the projects home run:

DEPRECATED!		git submodule add https://github.com/laverdet/node-autoload.git node_modules/autoload


How when cloning the repository, you can obtain all the dependencies:

		git submodule init
		git submodule update
Note: To remove a submodule from git repository:

- Delete the relevant line from the .gitmodules file.
- Delete the relevant section from .git/config.
- Run git rm --cached path_to_submodule (no trailing slash).

Example:

        git rm --cached node_modules/autoload

- Commit to git
- Delete the now untracked submodule files.

## Autpload

Use the `supervisor`! "It runs your program, and watches for code changes, so you can have hot-code reloading".

To install:
	
	npm install supervisor -g
	
To run, from the project's directory run:

	supervisor -p index.js
	
There is a bat file:

	run-server.bat
	

	



